from topic_7_oop.practice.class_2_0_organ import Organ


class Heart(Organ):
    def __init__(self, healthy, mass, pressure):
        super().__init__(healthy, mass)
        self.pressure = pressure

    def listen_to(self):
        if self.healthy:
            print("tok-tok-tok")
        else:
            print("bam-bam-bam")
