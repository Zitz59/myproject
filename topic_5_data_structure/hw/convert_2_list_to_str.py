def list_to_str(my_list: list, sep_0: str):
    """
    Функция list_to_str.

    Принимает 2 аргумента: список и разделитель (строка).

    Возвращает (строку полученную разделением элементов списка разделителем,
    количество разделителей в получившейся строке в квадрате).

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

    Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.
#
    Если список пуст, то возвращать пустой tuple().

    ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
    """

    if type(my_list) != list:
        return 'First arg must be list!'
    if type(sep_0) != str:
        return 'Second arg must be str!'
    if len(my_list) == 0:
        return tuple(my_list)
    else:
        my_list = [str(i) for i in my_list]
        string = sep_0.join(my_list)
    return string, string.count(sep_0) ** 2
