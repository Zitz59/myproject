# Test passed
def zip_names(name_list: list, surname_set: set):
    """
    Функция zip_names.

    Принимает 2 аргумента: список с именами и множество с фамилиями.

    Возвращает список с парами значений из каждого аргумента.#

    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

    Если list пуст, то возвращать строку 'Empty list!'.
    Если set пуст, то возвращать строку 'Empty set!'.

    Если list и set различного размера, обрезаем до минимального (стандартный zip).
    """
    if type(name_list) != list:
        return 'First arg must be list!'
    if type(surname_set) != set:
        return 'Second arg must be set!'
    if not name_list:
        return 'Empty list!'
    if not surname_set:
        return 'Empty set!'

    return list(zip(name_list, surname_set))
