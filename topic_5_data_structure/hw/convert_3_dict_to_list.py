# Test passed
def dict_to_list(my_dict: dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает кортеж: (
    список ключей,
    список значений,
    количество уникальных элементов в списке ключей,
    количество уникальных элементов в списке значений
    ).
#
    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
    """
    if type(my_dict) != dict:
        return 'Must be dict!'
    my_list = []
    keys = list(my_dict.keys())
    values = list(my_dict.values())
    my_list.append(keys)
    my_list.append(values)
    my_list.append(len(keys))
    my_list.append(len(set(values)))

    return tuple(my_list)

