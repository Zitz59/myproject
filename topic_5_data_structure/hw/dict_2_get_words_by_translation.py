# Test passed
def get_words_by_translation(my_dict, search_word):
    """
    Функция get_words_by_translation.
    
    Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (eng).#
    
    Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
    если нет, то ‘Can`t find English word: {word}’.
    
    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.
    
    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """
    if type(my_dict) != dict:
        return 'Dictionary must be dict!'
    if type(search_word) != str:
        return 'Word must be str!'
    if my_dict == {}:
        return 'Dictionary is empty!'
    if search_word == '':
        return 'Word is empty!'
    ru_tr_list = []

    for index, (ru_word, eng_word) in enumerate(my_dict.items()):
        if search_word in eng_word:
            ru_tr_list.append(ru_word)
    if ru_tr_list:
        return ru_tr_list
    else:
        return f'Can`t find English word: {search_word}'


if __name__ == '__main__':
    print(get_words_by_translation({'мама': ['mom', 'omo'], 'папа': 'dad'}, 'ada'))
