# Test passed
def get_translation_by_word(ru_eng_dict: dict, search_word: str):
    """
    Функция get_translation_by_word.
    
    Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (ru).
    
    Возвращает все варианты переводов (list), если такое слово есть в словаре,
    если нет, то ‘Can`t find Russian word: {word}’.
    
    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.
    #
    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """
    if ru_eng_dict == {}:
        return 'Dictionary is empty!'
    if type(ru_eng_dict) != dict:
        return 'Dictionary must be dict!'
    if type(search_word) != str:
        return 'Word must be str!'
    if search_word == '':
        return 'Word is empty!'
    return ru_eng_dict.get(search_word, f'Can`t find Russian word: {search_word}')
