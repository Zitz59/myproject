# Marina
def dict_to_list(my_dict: dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает список: [
        список (list) ключей,
        список (list) значений,
        количество элементов в списке ключей в степени 3,
        количество уникальных элементов в списке значений,
        хотя бы один ключ равен одному из значений (True | False).
    ]

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

    Если dict пуст, то возвращать [[], [], 0, 0, False].
    """

    if type(my_dict) != dict:
        return 'Must be dict!'

    my_list = []

    keys = list(my_dict.keys())
    values = list(my_dict.values())
    # x = len(keys) ** 3
    values_set = set(values)
    # values_set_len = len(set(values))

    my_list.append(keys)
    my_list.append(values)
    # my_list.append(x)
    my_list.append(len(keys) ** 3)
    # my_list.append(values_set_len)
    my_list.append(len(set(values)))

    # include_val = False

    for i in keys:
        if i in values_set:
            my_list.append(True)
            # include_val = True
            break

        # for j in values_set:
        #     if i == j:
        #         include_val = True
    else:
        my_list.append(False)

    # my_list.append(include_val)

    return my_list


if __name__ == '__main__':
    dict_to_list({1: 'a', 2: 'b'})
