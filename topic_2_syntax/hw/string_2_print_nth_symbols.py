def print_nth_symbols(mystring, n):
    if type(n) != int:
        print('Must be int!')
    elif n <= 0:
        print('Must be > 0!')
    elif n > len(mystring):
        print('')
    elif n:
        print(mystring[n::n])
