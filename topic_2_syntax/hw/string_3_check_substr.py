def check_substr(str_1, str_2):
    if len(str_1) == len(str_2):
        return False
    elif len(str_1) == 0 or len(str_2) == 0:  # add condition ==0 for len(str_1)
        return True
    elif len(str_1) > len(str_2) and str_2 in str_1 \
            or len(str_2) > len(str_1) and str_1 in str_2:  # add check for length
        return True
    else:
        return False
