def count_odd_num(n):
    if type(n) != int:
        return "Must be int!"

    if n <= 0:
        return "Must be > 0!"

    count = 0
    for i in str(n):
        if int(i) % 2 > 0 or int(i) % 2 < 0:
            count += 1
    return count
