def print_symbols_if(my_str):
    if len(my_str) == 0:
        print(str("Empty string!"))

    if len(my_str) > 5:
        print(my_str[:3] + my_str[-3:])

    elif len(my_str):
        print(my_str[0:1] * len(my_str))
