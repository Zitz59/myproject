def check_sum(a, b, c):
    if (a + b) == c:
        return True
    elif (b + c) == a:
        return True
    elif (c + a) == b:
        return True
    else:
        return False
