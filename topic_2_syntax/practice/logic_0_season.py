def season(season_number):
    """
    Функция season.

    Принимает 1 аргумент — номер месяца (от 1 до 12).
    Возвращает время года, которому этот месяц принадлежит
    ('Winter', 'Spring', 'Summer' или 'Fall').
    Если номер месяца не входит в диапазон от 1 до 12, то вернуть None.
    """

    if 3 <= season_number <= 5:
        return 'Spring'
    elif 6 <= season_number <= 8:
        return 'Summer'
    elif 9 <= season_number <= 11:
        return 'Fall'
    elif season_number == 12 or season_number == 1 or season_number == 2:
        return 'Winter'
    else:
        return None     # так и задумано


if __name__ == '__main__':
    print(season(4))
