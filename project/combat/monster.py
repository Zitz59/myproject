from project.combat.monster_type import MonsterType
from project.combat.monster_types_weakneses import monster_weaknesses_by_type as  weaknesses_by_type
from project.combat.body_part import BodyPart
from project.combat.monster_state import MonsterState


class Monster:
    def __init__(self,
                 name: str,
                 monster_type: MonsterType):
        self.name = name
        self.monster_type = monster_type
        self.weaknesses = weaknesses_by_type.get(monster_type, tuple())
        self.hp = 120
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 20
        self.state = MonsterState.READY

    def __str__(self):
        return f"Name : {self.name} | Type: {self.monster_type.name}\nHP: {self.hp}"

    def next_step_points(self,
                         next_attack: BodyPart,
                         next_defence: BodyPart):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: BodyPart,
                opponent_hit_power: int,
                opponent_type: MonsterType):
        if opponent_attack_point == BodyPart.NOTHING:
            return "Спасибо"
        if self.defence_point == opponent_attack_point:
            return "Ха-ха , мимо!!!"
        else:
            self.hp -= opponent_hit_power * (4 if opponent_type in self.weaknesses else 1)

            if self.hp <= 0:
                self.state = MonsterState.DEFEATED
                return "Уничтожен!"
            else:
                return "Ранен ,но не убит!"
