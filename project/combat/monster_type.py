from enum import Enum, auto


class MonsterType(Enum):
    PHYSIC = auto()
    DEATH = auto()
    LIVE = auto()
    CREEPY = auto()
    FUNNY = auto()
    POISON_FREE = auto()
    POISON = auto()
    CHAOS = auto()
    ORDER = auto()
    MAGIC = auto()
    WATER = auto()
    MENTAL = auto()
    STUPID = auto()
    FIRE = auto()

    @classmethod
    def min_value(cls):
        return cls.PHYSIC.value

    @classmethod
    def max_value(cls):
        return cls.STUPID.value
