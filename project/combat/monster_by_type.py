from project.combat.monster_type import MonsterType

monster_by_type = {

    MonsterType.MENTAL: {'Brain Blisterer': 'brain_blisterer.jpg',
                         'The Weed Wizard': 'the_weed_wizard.jpg',
                         },

    MonsterType.LIVE: {'Corpse Burner': 'corpse_burner.jpg',
                       'Mercenary Alchemist': 'mercenary_alchemist.jpg', },

    MonsterType.DEATH: {'Daemonic Servant': 'daemonic_servant.jpg',
                        'Vampyr Lord': 'vampyr_lord.jpg',
                        'Wampyr Duchess': 'wampyr_duchess.jpg'},


    MonsterType.CHAOS: {'Chaos Warrior': 'chaos_warrior.jpg',
                        'Daemonic Voivod': 'daemonic_voivod.jpg',},

    MonsterType.MAGIC: {'Fishoid Warlock': 'fishoid_warlock.jpg',
                        'Chicken Witch': 'chicken_witch.jpg', },

    MonsterType.PHYSIC: {'Wolf Man': 'wolf_man.jpg',
                         'Sex Dwarf': 'sex_dwarf.jpg',
                         'Nacht Dreck Raider': 'nacht_dreck_raider.jpg',
                         },

    MonsterType.POISON: {'Naga': 'naga.jpg',
                         'Void Witch': 'void_witch.jpg', },

MonsterType.WATER: {'Corpse Golem': 'corpse_golem.jpg',
                    'Goblin': 'goblin.jpg',},

    MonsterType.POISON_FREE: {'Plague Finder': 'plague_finder.jpg',
                              'Witch Smeller': 'witch_smeller.jpg',
                               },

    MonsterType.CREEPY: {'Ettin': 'ettin.jpg',
                         'Morbid Fighter': 'morbid_fighter.jpg'},

    MonsterType.FUNNY: {'Evil Puppet Show': 'evil_puppet_show.jpg',
                        'Fighting Outhouse': 'fighting_outhouse.jpg',
                        },

    MonsterType.STUPID: {'Pickle Goblon': 'pickle_goblon.jpg',
                         'Scarecrow': 'scarecrow.jpg',
                         },

    MonsterType.ORDER: {'Hinterlander': 'hinterlander.jpg',
                        'Bloodsport Brawler': 'bloodsport_brawler.jpg', },


    MonsterType.FIRE: {'Pork Fighter': 'pork_fighter.jpg',
                        'Troll': 'troll.jpg', }
}
