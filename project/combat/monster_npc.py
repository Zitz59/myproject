from project.combat.monster import Monster
from project.combat.monster_type import MonsterType
from project.combat.monster_by_type import monster_by_type
from project.combat.body_part import BodyPart
import random


class MonsterNPC(Monster):
    def __init__(self):
        rand_type_value = random.randint(MonsterType.min_value(), MonsterType.max_value())

        rand_monster_type = MonsterType(rand_type_value)

        rand_monster_name = random.choice(list(monster_by_type.get(rand_monster_type, {}).keys()))

        super().__init__(rand_monster_name, rand_monster_type)

    # для избежания проблем связанных с наследованием можно добавить аргумент-заглушку **kwargs
    # или можно переименовать метод
    # или игнорировать WARNING
    def next_step_points(self, **kwargs):
        attack_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        defence_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        super().next_step_points(next_attack=attack_point,
                                 next_defence=defence_point)


if __name__ == '__main__':
    monster_npc = MonsterNPC()
    monster_npc.next_step_points()
