from enum import Enum, auto


class MonsterState(Enum):
    READY = auto()
    DEFEATED = auto()

# MonsterState = Enum ('MonsterState', 'READY DEFEATED') короткая запись
