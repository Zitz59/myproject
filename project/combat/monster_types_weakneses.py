from project.combat.monster_type import MonsterType

monster_weaknesses_by_type = {

    MonsterType.MAGIC: {MonsterType.PHYSIC,
                        MonsterType.LIVE,
                        MonsterType.CHAOS,
                        MonsterType.MENTAL, },

    MonsterType.FUNNY: {MonsterType.CREEPY},

    MonsterType.CREEPY: {MonsterType.FUNNY},

    MonsterType.DEATH: {MonsterType.MAGIC,
                        MonsterType.LIVE,
                        },
    MonsterType.CHAOS: {MonsterType.ORDER},

    MonsterType.ORDER: {MonsterType.CHAOS},

    MonsterType.PHYSIC: {MonsterType.MAGIC,
                         MonsterType.MENTAL,
                         },
    MonsterType.FIRE: {MonsterType.WATER,
                       MonsterType.DEATH,
                       MonsterType.LIVE,
                       },
    MonsterType.WATER: {MonsterType.FIRE,
                        MonsterType.DEATH,
                        MonsterType.CHAOS,
                        },

    MonsterType.STUPID: {MonsterType.MENTAL,
                         MonsterType.MAGIC},
    MonsterType.POISON: {MonsterType.LIVE,
                         MonsterType.CHAOS,
                         MonsterType.MENTAL},

    MonsterType.POISON_FREE: {MonsterType.POISON,
                              MonsterType.CHAOS,
                              MonsterType.MAGIC}

}
