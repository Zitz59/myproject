from enum import Enum, auto

# победа поражение ничья
GameResult = Enum("GameResult", "W L D")
