import json

import telebot
from telebot import types
from telebot.types import Message

from project.combat.game_result import GameResult
from project.combat.monster import Monster
from project.combat.monster_by_type import monster_by_type
from project.combat.monster_npc import MonsterNPC
from project.combat.monster_type import MonsterType
from project.combat.body_part import BodyPart
from project.combat.monster_state import MonsterState

with open("./bot_token.txt", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

# {id:{'user_monster': Monster_obj, 'npc_monster'}
state = {}
# {id:{'wins': 0, 'loses' :6 'draws': 5,} ...}
statistics = {}

stat_file = "game_stat.json"

body_part_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                               one_time_keyboard=True,
                                               row_width=len(BodyPart))
body_part_keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in BodyPart])


def load_stat():
    print('Загрузка статистики...', end='')
    # TODO: add try except
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('завершена успешно!')
    except FileNotFoundError as my_error:
        statistics = {}
        print('файл не найден!')


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id, "Hi!\n /start для начала игры \n /stat для отображения статистики")


@bot.message_handler(commands=["stat"])
def stat(message):
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Вы ещё не сыграли ни одной игры"
    else:
        user_stat = "Твои результаты: \n"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat, )


@bot.message_handler(commands=['start'])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,
                     text="Готов начать бой?",
                     reply_markup=yes_no_keyboard)
    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         'Твой соперник величественно выползает из кустов....')
        create_npc(message)

        ask_user_about_monster_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         'Ok , подождём...')
    else:
        bot.send_message(message.from_user.id,
                         'Не знаю такого варианта ответа!')


def create_npc(message):
    print(f" Начало создания объекта NPC для chat id = {message.chat.id}")
    global state
    monster_npc = MonsterNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_monster'] = monster_npc

    npc_image_filename = monster_by_type[monster_npc.monster_type][monster_npc.name]

    with open(f"../images/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, monster_npc)
    print(f" Завершено создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_monster_type(message):
    print(f" Начало создания выбранного монстра для user = {message.chat.id}")
    markup = types.InlineKeyboardMarkup()

    # TODO: несколько кнопок в ряду

    for monster_type in MonsterType:
        markup.add(types.InlineKeyboardButton(text=monster_type.name,
                                              callback_data=f"monster_type_{monster_type.value}"))

    bot.send_message(message.chat.id, "Выбери тип монстра:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "monster_type_" in call.data)
def monster_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        monster_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, "Выбери своего персонажа:")

        ask_user_about_monster_by_type(monster_type_id, call.message)


def ask_user_about_monster_by_type(monster_type_id, message):
    monster_type = MonsterType(monster_type_id)
    monster_dict_by_type = monster_by_type.get(monster_type, {})

    for monster_name, monster_img in monster_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=monster_name,
                                              callback_data=f"monster_name_{monster_type_id}_{monster_name}"))
        with open(f"../images/{monster_img}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)

    print(f" Завершено создание выбранного монстра для user = {message.chat.id}")


@bot.callback_query_handler(func=lambda call: "monster_name_" in call.data)  # обработка нажатия кнопки выбора монстра
def monster_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        monster_type_id, monster_name = int(call_data_split[2]), call_data_split[3]

        create_user_monster(call.message, monster_type_id, monster_name)

        bot.send_message(call.message.chat.id, "Игра началась!")

        game_next_step(call.message)


def create_user_monster(message, monster_type_id, monster_name):
    global state
    user_monster = Monster(name=monster_name,
                           monster_type=MonsterType(monster_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_monster'] = user_monster

    image_filename = monster_by_type[user_monster.monster_type][user_monster.name]
    bot.send_message(message.chat.id, 'Вы выбрали:')
    with open(f"../images/{image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_monster)
    print(f" Завершено создание объекта NPC для chat id = {message.chat.id}")


def game_next_step(message: Message):
    bot.send_message(message.chat.id, "Выбор точки для защиты",
                     reply_markup=body_part_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Выбор точки для удара:",
                         reply_markup=body_part_keyboard)
        bot.register_next_step_handler(message, reply_attack, defend_body_part=message.text)


def reply_attack(message: Message, defend_body_part: str):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_body_part = message.text

        global state
        # Обращение по ключу к вложенному словарю для получения объекта персонажа пользователя
        user_monster = state[message.chat.id]['user_monster']
        # Обращение по ключу к вложенному словарю для получения объекта персонажа бота
        monster_npc = state[message.chat.id]['npc_monster']
        # конвертировать строку хранящуюся в объекте attack_body_part в правильный тип Enum BodyPart
        user_monster.next_step_points(next_attack=BodyPart[attack_body_part],
                                      next_defence=BodyPart[defend_body_part])
        # конвертировать строку хранящуюся в объекте defend_body_part в правильный тип Enum BodyPart
        monster_npc.next_step_points()

        game_step(message, user_monster, monster_npc)


def game_step(message: Message, user_monster: Monster, monster_npc: Monster):
    comment_npc = monster_npc.get_hit(opponent_attack_point=user_monster.attack_point,
                                      opponent_hit_power=user_monster.hit_power,
                                      opponent_type=user_monster.monster_type)

    bot.send_message(message.chat.id, f"NPC dude: {comment_npc}\nHP:{monster_npc.hp}")
    comment_user = user_monster.get_hit(opponent_attack_point=monster_npc.attack_point,
                                        opponent_hit_power=monster_npc.hit_power,
                                        opponent_type=monster_npc.monster_type)
    bot.send_message(message.chat.id, f"Your dude : {comment_user}\nHP:{user_monster.hp}")

    if monster_npc.state == MonsterState.READY and user_monster.state == MonsterState.READY:
        bot.send_message(message.chat.id, "Продолжаем бой!")
        game_next_step(message)

    elif monster_npc.state == MonsterState.DEFEATED and user_monster.state == MonsterState.DEFEATED:
        bot.send_message(message.chat.id, "Ничья : Злобно рыча вы отползаете друг от друга!")
        update_save_stat(message.chat.id, GameResult.D)
    elif monster_npc.state == MonsterState.DEFEATED:
        bot.send_message(message.chat.id, "Ты победил! Можешь выдрать пару золотых зубов врага ;)")
        update_save_stat(message.chat.id, GameResult.W)
    elif user_monster.state == MonsterState.DEFEATED:
        bot.send_message(message.chat.id, "Ты проиграл и тебя сожрали!")
        update_save_stat(message.chat.id, GameResult.L)


def update_save_stat(user_id, result: GameResult):
    print("Обновление статистики", end="...")
    global statistics

    if statistics.get(user_id, None) is None:
        statistics[user_id] = {}

    if result == GameResult.W:
        statistics[user_id]['W'] = statistics[user_id].get('W', 0) + 1
    elif result == GameResult.L:
        statistics[user_id]['L'] = statistics[user_id].get('L', 0) + 1
    elif result == GameResult.D:
        statistics[user_id]['D'] = statistics[user_id].get('D', 0) + 1
    else:
        print(f"Не существует результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("завершено!")


if __name__ == '__main__':
    load_stat()
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
