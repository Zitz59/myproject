import random
from project.combat.monster_npc import MonsterNPC
from project.combat.monster_state import MonsterState
from project.combat.monster_type import MonsterType
from project.combat.monster_types_weakneses import monster_weaknesses_by_type




class TestMonsterNPCClass:
    monster_name = 'Sex Dwarf'     # определить после первого запуска теста
    monster_type = MonsterType.PHYSIC    # определить после второго запуска теста
    max_hp = 120



    def setup_method(self, method):
        """"Этот метод будет выполняться перед каждым тестом из данного класса"""
        # установить точку отчёта для случайных чисел
        random.seed(123)

    def test_init(self):
        monster_test = MonsterNPC()

        assert monster_test.name == self.__class__.monster_name
        assert monster_test.monster_type == self.__class__.monster_type
        assert monster_test.weaknesses == monster_weaknesses_by_type[self.__class__.monster_type]
        assert monster_test.hp == self.__class__.max_hp
        assert monster_test.attack_point is None
        assert monster_test.defence_point is None
        assert monster_test.hit_power == 20
        assert monster_test.state == MonsterState.READY

    def test_str(self):
        monster_test = MonsterNPC()

        assert str(monster_test) == f"Name : {self.__class__.monster_name} | " \
                                    f"Type: {self.__class__.monster_type.name}\n" \
                                    f"HP: {self.__class__.max_hp}"
