"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flower="Ромашка", color="белый", price=10.25):
    if isinstance(flower, str) and isinstance(color, str) and 0 < price < 10000:
        print("{}|{}|{}".format(flower, color, price))


if __name__ == '__main__':
    flower_with_default_vals()
    flower_with_default_vals('Алоэ', 'синий')
    flower_with_default_vals('Цветок-людоед', color="белый", price=9999)
    flower_with_default_vals(flower='Ромашка', color='зелёный', price=0.44)
    flower_with_default_vals('Цветок-программист', 'белый', 53.48)

