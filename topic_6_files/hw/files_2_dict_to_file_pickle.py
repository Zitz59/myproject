import pickle


def save_dict_to_file_pickle(file_name, save_dict):
    """
    Функция save_dict_to_file_pickle.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
    """
    with open(file_name, 'wb') as file:
        pickle.dump(save_dict, file)


if __name__ == '__main__':
    save_dict_to_file_pickle('simple_file_2',
                             {1: 653, 2: 'avst', 3: 80, 4: 'te'})

    with open('simple_file_2', 'rb') as file:
        dict_load = pickle.load(file)
        print(type(dict_load), dict_load)
