def read_str_from_file(filename):
    """
    Функция read_str_from_file.

    Принимает 1 аргумент: строка (название файла или полный путь к файлу).

    Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
    """

    f = open(filename, 'r', encoding='utf-8')
    print(f.read())
    f.close()


if __name__ == '__main__':
    filename = 'testfile_3.txt'
    read_str_from_file(filename)
