def save_dict_to_file_classic(file_path, my_dict):
    """
    Функция save_dict_to_file_classic.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл.
    """
    with open(file_path, 'w') as file:
        file.write(str(my_dict))

if __name__ == '__main__':
    save_dict_to_file_classic('simple_file_1',
                              {1: 'mdld', 2: 'avpop', 3: 513113, 4: 'te'})