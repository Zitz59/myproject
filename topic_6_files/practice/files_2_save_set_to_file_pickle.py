# Марина

import pickle


def save_set_to_file_pickle(filename: str, save_set: set):
    """
    Функция save_set_to_file_pickle.

    Принимает 2 аргумента:
        строка (название файла или полный путь к файлу),
        множество (для сохранения).

    Сохраняет множество в файл.

    Загрузить множество (прочитать файл) и проверить его на корректность и тип.
    """

    with open(filename, 'wb') as f:
        pickle.dump(save_set, f)

    # "классический" способ для разнообразия (open/close)
    file = open(filename, 'rb')
    my_set = pickle.load(file)
    file.close()

    print(f"my_set == save_set: {my_set == save_set}")
    print(f"type(my_set): {type(my_set)}")


if __name__ == '__main__':
    save_set_to_file_pickle('task_set',
                            {1, 2, 3, 4})

